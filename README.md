# MapR-Nagios
##check_mapr_yarn
Plugin to check the Yarn Resource Manager using MapR CLI. This plugin checks the following:

*  Running Apps
*  Queued Apps
*  Memory used, memory maximum
*  Cores used, cores maximum
*  Disk used, disk maximum

If you are only interested in performance data, run the plugin without options.


To get more information on the options use the following:
```
$ ./check_mapr_yarn -h
```

##check_mapr_utilization
Plugin to check cluster resources using the MapR CLI. This plugin checks the following:

*  CPU utilization in %
*  Memory used, memory maximum
*  Cores used, cores maximum
*  Disk used, disk maximum

If you are only interested in performance data, run the plugin without options.


To get more information on the options use the following:
```
$ ./check_mapr_utilization -h
```

## Requirements
These plugins use jq to parse the json output. For JQ see: https://stedolan.github.io/jq/download/

Two nagios plugins to get cluster information into Nagios. 

## Nagios config
This is an example nagios config on how to use these plugins.
```
define command{
        command_name    check_mapr_yarn
        command_line    $USER1$/check_mapr_yarn
        }

define command{
        command_name    check_mapr_utilization
        command_line    $USER1$/check_mapr_utilization
        }

define host{
        use                     linux-server
        host_name               mapr
        alias                   mapr
        address                 127.0.0.1
        }

define service{
        use                             local-service
        host_name                       mapr
        service_description             mapr yarn
        check_command                   check_mapr_yarn
        check_interval                  1
        }

define service{
        use                             local-service
        host_name                       mapr
        service_description             mapr utilization
        check_command                   check_mapr_utilization
        check_interval                  1
        }
```
        
##Templates
Two pnp4nagios templates are added. They add a line for maximum amount of cores, memory and disk.
Place the templates in: 
```
/usr/share/nagios/html/pnp4nagios/templates
```
Make sure to give the template the same name as the command.
